mgrep
=====

Personal grep wrapper for searching logs and other large caches of text files
lazily and interactively.

I wrote this to search my IRC logs in a quick and lazy way, but you could use
it for any sort of quick-recursive-grep interactive task, hopefully.

"mgrep" is short for "my grep".

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under [MIT License][2].

[1]: https://sanctum.geek.nz/
[2]: http://opensource.org/licenses/MIT
